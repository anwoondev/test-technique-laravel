<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Scripts -->
        <script src="{{ asset('js/app.js') }}" defer></script>
    
        <!-- Styles -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">

        <title>Test technique - Laravel 9</title>
    </head>
    <body>
        <div class="container">
            <h1 class="mt-5 text-center">Posts Instagram</h1>
            <div class="row row-cols-1 row-cols-md-3 g-4 mt-5">
                @for ($i = 0; $i < count($values); $i++)
                    <div class="col">
                        <div class="card h-100">
                        <img src="{{ $values[$i]['image'] }}" class="card-img-top" alt="{{ $values[$i]['caption'] }}">
                        <div class="card-body">
                            <p class="card-text">{{ $values[$i]['caption'] }}</p>
                        </div>
                        </div>
                    </div>
                @endfor
            </div>
        </div>
    </body>
</html>
