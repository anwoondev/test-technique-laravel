<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller;
use App\Services\InstagramService;

class InstagramController extends Controller
{
    /**
     * The instagram service instance.
     * 
     * @var \App\Services\InstagramService
     */
    protected $instagram_service;

    /**
     * Construct of Instagram Controller.
     * 
     * @var \App\Services\InstagramService $instagram_service
     *   This is instagram service.
     */
    public function __construct(InstagramService $instagram_service)
    {
        $this->instagram_service = $instagram_service;
    }

    /**
     * Index of instagram posts.
     */
    public function index()
    {
        $instagram_values = $this->instagram_service->getMedias();
        return view('instagram', ['values' => $instagram_values]);
    }
}
