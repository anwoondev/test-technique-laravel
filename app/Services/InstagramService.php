<?php

namespace App\Services;

use Illuminate\Support\Facades\Cache;

class InstagramService
{
    private $app_pseudo = '';

    function __construct(string $app_pseudo)
    {
        $this->app_pseudo = $app_pseudo;
    }

    /**
     * Get medias from instagram.
     * 
     * @return array
     */
    public function getMedias()
    {
        $values = Cache::get('instagram');

        if (!$values)
        {
            $instagram = new \InstagramScraper\Instagram(new \GuzzleHttp\Client());
            $values = $this->cleanValues($instagram->getMedias($this->app_pseudo));
            Cache::set('instagram', $values, 3600);
        }

        return $values;
    }

    public function cleanValues($medias)
    {
        $clean_medias = [];
        $client = new \GuzzleHttp\Client();
        foreach ($medias as $media)
        {
            $response = $client->request('GET', $media->getImageHighResolutionUrl());
            $content = base64_encode($response->getBody()->getContents());
    
            $clean_medias[] = [
                'caption' => $media->getCaption(),
                'image' => "data:image/jpeg;base64,$content",
            ];
        }

        return $clean_medias;
    }

}