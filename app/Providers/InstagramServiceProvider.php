<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\InstagramService;
use Barryvdh\Debugbar\Facades\Debugbar;

class InstagramServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(InstagramService::class, function ($app) {
            return new InstagramService(config('app')['instagram_pseudo']);
        });
    }
}
